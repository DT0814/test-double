package stub;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;


import java.util.Arrays;

import static org.mockito.Mockito.*;

public class GradeServiceTest {
    /* 需求描述：
    编写GradeService类的单元测试，单元测试calculateAverageGrades方法
    * */
    @Mock
    GradeSystem gradeSystem;
    GradeService gradeService;

    @BeforeEach
    void setUp() {
        gradeSystem = mock(GradeSystem.class);
        gradeService = new GradeService(gradeSystem);
    }

    @Test
    public void shouldReturn90WhenCalculateStudentAverageGradeAndGradeIs80And90And100() {
        when(gradeSystem.gradesFor(1)).thenReturn(Arrays.asList(80.0,90.0,100.0));
        double res = gradeService.calculateAverageGrades(1);
        verify(gradeSystem).gradesFor(1);
        Assertions.assertEquals(res,90.0);
    }

}
